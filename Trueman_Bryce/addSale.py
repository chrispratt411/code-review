def record_sale(self, cartTotal):
		token = self.cleaned_data.get('stripe_token')
		print(token)
		# eventually change this to the setting.py thing
		stripe.api_key = settings.STRIPE_SECRET_KEY

		# you have to multiply by 100 for stripe to accept it
		tempCartTotal = Decimal(cartTotal) * 100
		print('`````` tempCartTotal: ', tempCartTotal)

		# Charge the user's card:
		charge = stripe.Charge.create(
		  amount=int(tempCartTotal),
		  currency='usd',
		  description='Charge to: ' + self.request.user.username,
		  source=token,
		)

		print('^^^^^^^^^ charge ^^^^^^^^^^^^^^^^', charge)

		print(charge['amount'])
		print(charge['id'])


		###### Creating a new Sale object ########
		newSale = cmod.Sale()
		newSale.user = self.request.user
		newSale.stripeID = charge['id']
		newSale.subTotal = self.request.user.calc_Subtotal()
		newSale.taxPrice = self.request.user.calc_Tax(self.request.user.calc_Subtotal())
		newSale.shippingPrice = self.request.user.calc_Shipping()
		newSale.totalPrice = charge['amount'] / 100
		newSale.street_address = self.request.session['number'] + ' ' + self.request.session['street']
		newSale.city = self.request.session['city']
		newSale.state = self.request.session['state']
		newSale.zip_code = self.request.session['zipcode']
		newSale.save()

		###### Creating a new SaleItem object ########
		scItems = self.request.user.retrieveCart()
		for s in scItems:
			newSaleItem = cmod.SaleItem()
			newSaleItem.product = s.product
			newSaleItem.sale = newSale
			newSaleItem.itemPrice = s.product.price
			newSaleItem.quantity = s.quantity
			newSaleItem.save()

		###### Creating a new Payment object ########
		newPayment = cmod.Payment()
		newPayment.sale = newSale
		newPayment.amount = charge['amount']
		newPayment.stripeID = charge['id']
		newPayment.save()

		###### Save the ID to a request variable to be accessed when redirecting to receipt.html ########
		self.request.session['saleID'] = newSale.id

		###### update the BulkProduct quantity and UniqueProduct availability #######
		for s in scItems:
			tempProduct = s.product
			if hasattr(s.product, 'quantity'):
				tempProduct.quantity = tempProduct.quantity - s.quantity
			else:
				tempProduct.available = False
			tempProduct.save()