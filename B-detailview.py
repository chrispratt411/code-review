from django.conf import settings
from django_mako_plus import view_function
from datetime import datetime
from .. import dmp_render, dmp_render_to_string
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required, permission_required
from catalog import models as cmod
from django import forms
from django.http import HttpResponseRedirect
from formlib.form import FormMixIn

@view_function
def process_request(request):
	# pull all products from the DB
	print('>>>>> at top of detailview')

	# this needs to be moved to base.py eventually
	# this just grabs the last five to be displayed in the template
	try:

		# adding product that was just clicked to lastfive
		# grab the current product based on id in the url
		product = cmod.Product.objects.get(id=request.urlparams[0])
		# grab the lastfive from the session and put it into a list
		lastfive = request.last5
		# append the product.id to the list
		lastfive.insert(0, product.id)
		# check to make sure the list only contains 5, when it hits 6, pop an item off
		if len(lastfive) == 6:
			lastfive.pop()
		request.last5 = list(lastfive)


		# lastfive = list(map(lambda item: cmod.Product.objects.get(id=request.urlparams[0]), lastfive))
		# request.last5 = lastfive
		# print(request.last5)

	except cmod.Product.DoesNotExist:
		return HttpResponseRedirect('/catalog/items')


	context = {
		'product': product,
		'lastfive': lastfDisplay,
	}
	return dmp_render(request, 'detailview.html', context)
