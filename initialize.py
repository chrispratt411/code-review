from django.core import management
from django.db import connection
from datetime import datetime
from datetime import date
import os, os.path, sys

import os
os.environ['DJANGO_SETTINGS_MODULE'] = 'fomo.settings'


# intialize the django env

import django
django.setup()

# drop and recreate the database tables
print()
print('Living on the edge!  Dropping the current database tables.')
with connection.cursor() as cursor:
    cursor.execute("DROP SCHEMA public CASCADE")
    cursor.execute("CREATE SCHEMA public")
    cursor.execute("GRANT ALL ON SCHEMA public TO postgres")
    cursor.execute("GRANT ALL ON SCHEMA public TO public")

# make the migrations and migrate
management.call_command('makemigrations')
management.call_command('migrate')


from account.models import FomoUser

u1 = FomoUser()
u1.set_password('temp123')
u1.last_login = datetime(2017, 1, 25)
u1.is_superuser = False
u1.username = 'first_user'
u1.first_name = 'Bryce'
u1.last_name = 'Trueman'
u1.email = 'truemanbryce@gmail.com'
u1.is_staff = False
u1.is_active = True
u1.datejoined = datetime(2017, 1, 25)
u1.street_address = '5146 Camden Road'
u1.city = 'Rocklin'
u1.state = 'CA'
u1.zip_code = '95765'
u1.phone = '5305707971'
u1.birthday = datetime(1996, 2, 12)
u1.save()

u2 = FomoUser()
u2.set_password('temp123')
u2.last_login = datetime(2014, 1, 25)
u2.is_superuser = False
u2.username = 'second_user'
u2.first_name = 'Joe'
u2.last_name = 'Shmo'
u2.email = 'joe_shmo@gmail.com'
u2.is_staff = False
u2.is_active = True
u2.datejoined = datetime(2012, 1, 25)
u2.street_address = '1234 Random Road'
u2.city = 'Citrus Heights'
u2.state = 'CA'
u2.zip_code = '95610'
u2.phone = '1234567123'
u2.birthday = datetime(1998, 4, 19)
u2.save()

u3 = FomoUser()
u3.set_password('temp123')
u3.last_login = datetime(2015, 2, 15)
u3.is_superuser = False
u3.username = 'third_user'
u3.first_name = 'Ben'
u3.last_name = 'Nelson'
u3.email = 'ben_nelson@gmail.com'
u3.is_staff = False
u3.is_active = True
u3.datejoined = datetime(2010, 11, 5)
u3.street_address = '1234 Random Road'
u3.city = 'Boise'
u3.state = 'ID'
u3.zip_code = '12345'
u3.phone = '5724095821'
u3.birthday = datetime(1995, 6, 3)
u3.save()

# query all with different query options (3-5 examples)

# user = FomoUser.objects.get(1)
# users = FomoUser.objects.filter(id__lt=5)
# users = FomoUser.objects.exclude(id__gt=100)
# users = FomoUser.objects.filter(zip_code__contains='12345')





#
# # creates a new object (row) in the database
# q = Question()
# q.question_text = "Is it snowing today?"
# q.pub_date = datetime(2017, 6, 1, 5, 0)
# q.save()
#
# c1 = Choice()
# c1.question = q
# c1.choice.text = 'Yes'
# c1.save()
#
#
# # grabs it from the database based on id
# q1 = Question.objects.get(id=3)
#
# # grabs it from the database based on data value
# q2 = Question.objects.filter(question_text='Is it snowing today?')
# for q1 in questions:
#     print(q1.question_text)
#
# # grabs all of the question objects
# questions = Question.objects.all()
#
#
# # deletes a question
# q1.delete()
#
# # grabs those with id less than 5
# questions = Question.objects.filter(id__lt=5)
#
# # opposite of above, a not statement
# questions = Question.objects.exclude(id__lt=5)
#
# # get those not under 5, and contains the word today
# questions = Question.objects.exclude(id__lt=5).filter(question_text__contains='today')
#
#
#
#
#
# # to run this file from cmd
# # python initialize.py
