
def LastFiveMiddleware(get_response):
  # One-time conifg and init

  def middleware(request):
    # pull last 5 list out of the session
    last5_list = request.session.get('last5')

    if last5_list is None:
      last5_list = []

    request.last5 = last5_list
    
    # let django continue
    response = get_response(request)

    request.session['last5'] = request.last5[:5]
    
    return response

  return middleware