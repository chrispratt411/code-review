from django.db import models
from django.contrib.auth.models import AbstractUser
from datetime import datetime

class FomoUser(AbstractUser):
  '''
    The main user authentication class. 
    It has a bunch of methods and properties which
    are inherited from AbstractUser
  '''
  # inheriting from super
  STATE_CHOICES = (
    ('AL', 'Alabama'),
    ('AZ', 'Arizona'),
    ('AR', 'Arkansas'),
    ('CA', 'California'),
    ('CO', 'Colorado'),
    ('CT', 'Connecticut'),
    ('DE', 'Delaware'),
    ('DC', 'District of Columbia'),
    ('FL', 'Florida'),
    ('GA', 'Georgia'),
    ('ID', 'Idaho'),
    ('IL', 'Illinois'),
    ('IN', 'Indiana'),
    ('IA', 'Iowa'),
    ('KS', 'Kansas'),
    ('KY', 'Kentucky'),
    ('LA', 'Louisiana'),
    ('ME', 'Maine'),
    ('MD', 'Maryland'),
    ('MA', 'Massachusetts'),
    ('MI', 'Michigan'),
    ('MN', 'Minnesota'),
    ('MS', 'Mississippi'),
    ('MO', 'Missouri'),
    ('MT', 'Montana'),
    ('NE', 'Nebraska'),
    ('NV', 'Nevada'),
    ('NH', 'New Hampshire'),
    ('NJ', 'New Jersey'),
    ('NM', 'New Mexico'),
    ('NY', 'New York'),
    ('NC', 'North Carolina'),
    ('ND', 'North Dakota'),
    ('OH', 'Ohio'),
    ('OK', 'Oklahoma'),
    ('OR', 'Oregon'),
    ('PA', 'Pennsylvania'),
    ('RI', 'Rhode Island'),
    ('SC', 'South Carolina'),
    ('SD', 'South Dakota'),
    ('TN', 'Tennessee'),
    ('TX', 'Texas'),
    ('UT', 'Utah'),
    ('VT', 'Vermont'),
    ('VA', 'Virginia'),
    ('WA', 'Washington'),
    ('WV', 'West Virginia'),
    ('WI', 'Wisconsin'),
    ('WY', 'Wyoming'))
  street_address = models.CharField(max_length=255)
  city = models.CharField(max_length=50)
  state = models.CharField(max_length=2, choices=STATE_CHOICES)
  zip_code = models.CharField(max_length=10)
  phone = models.CharField(max_length=20)
  birthday = models.DateTimeField()
  def get_age(self):
    date = datetime.now()
    diff = date - self.birthday
    return diff.days / 365.25
  def record_sale(self, charge, address):
    #Charge is a stripe charge object
    #Address is the corrected geocoded address
    #First, create the sale
    sale = cmod.Sale()
    sale.user = self
    sale.tax_rate = decimal.Decimal('0.0725')
    sale.shipping = decimal.Decimal('10')
    sale.discount_rate = decimal.Decimal('0')
    sale.discount_amount = decimal.Decimal('0')
    sale.address = address

    sale.save()

    #Next, create the sale items from the cart items
    for item in self.cartitem_set.all():
      saleItem = cmod.SaleItem()
      saleItem.sale = sale
      saleItem.product = item.product
      saleItem.quantity = item.quantity
      saleItem.discount_rate = decimal.Decimal('0')
      saleItem.discount_amount = decimal.Decimal('0')

      saleItem.save()

      item.delete()

    #Finally, record the payment
    payment = cmod.Payment()
    payment.sale = sale
    payment.stripe_charge = charge.id

    payment.save()