#!/usr/bin/env python3

from django.core import management
from django.db import connection
from datetime import datetime
import os, os.path, sys


# ensure the user really wants to do this
areyousure = input('''
  You are about to drop and recreate the entire database.
  All data are about to be deleted.  Use of this script
  may cause itching, vertigo, dizziness, tingling in
  extremities, loss of balance or coordination, slurred
  speech, temporary zoobie syndrome, longer lines at the
  testing center, changed passwords in Learning Suite, or
  uncertainty about whether to call your professor
  'Brother' or 'Doctor'.

  Please type 'yes' to confirm the data destruction: ''')
if areyousure.lower() != 'yes':
    print()
    print('  Wise choice.')
    sys.exit(1)

# initialize the django environment
os.environ['DJANGO_SETTINGS_MODULE'] = 'fomo.settings'
import django
django.setup()


# drop and recreate the database tables
print()
print('Living on the edge!  Dropping the current database tables.')
with connection.cursor() as cursor:
    cursor.execute("DROP SCHEMA public CASCADE")
    cursor.execute("CREATE SCHEMA public")
    cursor.execute("GRANT ALL ON SCHEMA public TO fomo")
    cursor.execute("GRANT ALL ON SCHEMA public TO public")

# make the migrations and migrate
management.call_command('makemigrations')
management.call_command('migrate')


# imports for our project
from account.models import FomoUser

# Make 3 users
u1 = FomoUser()
u1.username = 'lskywalker'
u1.first_name = 'Luke'
u1.last_name = 'Skywalker'
u1.email = 'usetheforce@gmail.com'
u1.street_address = '121 Skywalker Ranch'
u1.city = 'Tatooine'
u1.state = 'CA'
u1.zip_code = 90131
u1.phone = '555-555-5555'
u1.birthday = datetime(1965, 7, 12)
u1.set_password('toshi_station')
u1.save()

u2 = FomoUser()
u2.username = 'lorgana'
u2.first_name = 'Leia'
u2.last_name = 'Organa'
u2.email = 'herhighness@gmail.com'
u2.street_address = 'Nowhere'
u2.city = 'Alderaan'
u2.state = 'CA'
u2.zip_code = 94182
u2.phone = '555-555-5555'
u2.birthday = datetime(1965, 7, 12)
u2.set_password('i_love_you')
u2.save()

u3 = FomoUser()
u3.username = 'hsolo'
u3.first_name = 'Han'
u3.last_name = 'Solo'
u3.email = 'scruffynerfherder@gmail.com'
u3.street_address = 'Hyperspace'
u3.city = 'Millenium Falcon'
u3.state = 'CA'
u3.zip_code = 93491
u3.phone = '555-555-5555'
u3.birthday = datetime(1962, 6, 6)
u3.set_password('i_know')
u3.save()


# Pull all / some with different query options
# By ID
print(FomoUser.objects.get(pk=1))
# By attribute
print(FomoUser.objects.filter(username='hsolo'))
# Find all
print(FomoUser.objects.all())
