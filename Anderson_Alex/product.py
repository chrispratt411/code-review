from django_mako_plus import view_function
from .. import dmp_render
from datetime import datetime
from django_mako_plus import view_function
from homepage import pageVars
from catalog import models as cmod


@view_function
def process_request(request):
  try:
    product = cmod.Product.objects.get(id=request.urlparams[0])
  except cmod.Product.DoesNotExist:
    return HttpResponseRedirect('/catalog')

  context = pageVars.getPageVars()
  
  last5 = request.last5
  
  last5Prods = list(map(lambda item: cmod.Product.objects.get(id=item), last5))

  context['product'] = product
  context['last5'] = last5Prods

  if product.id in last5:
    removeProduct = last5.index(product.id)
    del last5[removeProduct]
    
  last5.insert(0,product.id)

  return dmp_render(request, 'product.html', context)
