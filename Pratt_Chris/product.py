from django.conf import settings
from django_mako_plus import view_function
from datetime import datetime
from .. import dmp_render, dmp_render_to_string
from catalog import models as catalog_models
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import permission_required

@view_function
def process_request(request):
    # pull all products from the DB
    product = catalog_models.Product.objects.get(id=request.urlparams[0])
    products = catalog_models.Product.objects.order_by('category').all()

    last_five = request.last5
    inalready = False
    inalreadyIndex = 0

    for p in last_five:
        if p == product.id:
            inalready = True
            inalreadyIndex = last_five.index(p)

    if inalready:
        last_five.pop(inalreadyIndex)

    last_five.insert(0, product.id)

    while len(last_five) > 5:
        last_five.pop()
    request.last5 = list(last_five)

    fiveProducts = []

    for p in last_five:
        fiveProducts.insert(last_five.index(p), products.get(id=p).name)

    print('>>>>>> ', product)

    context = {
        'product': product,
        'last_five': last_five,
        'products': products,
    }

    return dmp_render(request, 'product.html', context)

@view_function
def modal(request):
    product = catalog_models.Product.objects.get(id=request.urlparams[0])

    print('>>>>>> ', product)

    context = {
        'product': product,
    }

    return dmp_render(request, 'productModal.html', context)
