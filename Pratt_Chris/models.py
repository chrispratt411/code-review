from django.db import models

# Create your models here.
from django.db import models
from django.contrib.auth.models import AbstractUser
from datetime import datetime

# Create your models here.
class FomoUser(AbstractUser):
    # inheriting from super - id, first_name, last_name, email
    street_address = models.TextField(null=True, blank=True)
    city = models.TextField(null=True, blank=True)
    state = models.TextField(null=True, blank=True)
    zip_code = models.TextField(null=True, blank=True)
    phone = models.TextField(null=True, blank=True)
    birthdate = models.DateField(null=True, blank=True)

    def get_age(self):
        return datetime.now() - birthdate
