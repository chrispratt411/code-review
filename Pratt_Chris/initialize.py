import os
os.environ['DJANGO_SETTINGS_MODULE'] = 'mysite.settings'

import django
django.setup()

from account.models import FomoUser

user1 = FomoUser()
user1.username = 'chrispratt'
user1.first_name = 'Chris'
user1.last_name = 'Pratt'
user1.email = 'chrispratt@gmail.com'
user1.set_password('chrispratt4ever')
user1.street_address = '1234 Grand Ave'
user1.city = 'Pasimaquadee'
user1.state = 'Ohio'
user1.zip_code = '54432'
user1.phone = '8163242315'
user1.birthdate = (1990, 10, 3)
user1.save()

user2 = FomoUser()
user2.username = 'devinbennett'
user2.first_name = 'Devin'
user2.set_password('chrispratt4ever')
user2.last_name = 'Bennett'
user2.email = 'chrispratt@gmail.com'
user2.street_address = '1785 Grand Ave'
user2.city = 'Pasimaquadee'
user2.state = 'Ohio'
user2.zip_code = '54432'
user2.phone = '8012345489'
user2.birthdate = (1992, 2, 7)
user2.save()

user3 = FomoUser()
user3.username = 'brycetrueman'
user3.first_name = 'Bryce'
user3.set_password('chrispratt4ever')
user3.last_name = 'Trueman'
user3.email = 'truemanbryce@gmail.com'
user3.street_address = '1900 Grand Ave'
user3.city = 'Pasimaquadee'
user3.state = 'Ohio'
user3.zip_code = '54432'
user3.phone = '8765435435'
user3.birthdate = (1996, 10, 26)
user3.save()

user4 = FomoUser()
user4.username = 'stephcurry'
user4.first_name = 'Stephen'
user4.set_password('chrispratt4ever')
user4.last_name = 'Curry'
user4.email = 'stephcurry@gmail.com'
user4.street_address = '1234 Main Street'
user4.city = 'Oakland'
user4.state = 'California'
user4.zip_code = '12345'
user4.phone = '9991234839'
user4.birthdate = (1989, 2, 11)
user4.save()


users_notfirst = FomoUser.objects.filter(id__gt=1)
users_c = FomoUser.objects.filter(username__startswith="c")
users_first_alphabet = FomoUser.objects.order_by('first_name')[0]
user_oldest = FomoUser.objects.order_by('birthdate')[0]

print(users_notfirst, users_c, users_first_alphabet, user_oldest)
