from django.db import models
from django.contrib.auth.models import AbstractUser
from datetime import datetime

# Contact_Choices = [
# ['text','Text']
# ['email','Email']
# ['voice','Voice']
#
# ]


# Define models here
class FomoUser(AbstractUser):
    '''This is my class to create my FomoUser database'''
    street_address = models.TextField(null= True)
    city = models.TextField(null= True)
    state  = models.TextField(null= True)
    zip_code = models.TextField(null=True)
    phone  = models.TextField(null=True)
    birthday  = models.DateTimeField('birthday')
    pref_contact = models.TextField(null=True,blank=True)
    #pref_contact = models.TextField(null=True,blank=True,Choices = Contact_Choices)
#test
# inheriting from super
# id
# first_name
# last_name
# email
