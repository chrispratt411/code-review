#!python

#!/usr/bin/env python3

from django.core import management
from django.db import connection
from datetime import datetime
import os, os.path, sys


# ensure the user really wants to do this
areyousure = input('''
  You are about to drop and recreate the entire database.
  All data are about to be deleted.  Use of this script
  may cause itching, vertigo, dizziness, tingling in
  extremities, loss of balance or coordination, slurred
  speech, temporary zoobie syndrome, longer lines at the
  testing center, changed passwords in Learning Suite, or
  uncertainty about whether to call your professor
  'Brother' or 'Doctor'.

  Please type 'yes' to confirm the data destruction: ''')
if areyousure.lower() != 'yes':
    print()
    print('  Wise choice.')
    sys.exit(1)

# initialize the django environment
os.environ['DJANGO_SETTINGS_MODULE'] = 'fomo.settings'
import django
django.setup()


# drop and recreate the database tables
print()
print('Living on the edge!  Dropping the current database tables.')
with connection.cursor() as cursor:
    cursor.execute("DROP SCHEMA public CASCADE")
    cursor.execute("CREATE SCHEMA public")
    cursor.execute("GRANT ALL ON SCHEMA public TO fomo")
    cursor.execute("GRANT ALL ON SCHEMA public TO public")

# make the migrations and migrate
management.call_command('makemigrations')
management.call_command('migrate')


from account.models import FomoUser

 Question q = new Question();



user1 = FomoUser()
user1.first_name = 'Devin'
user1.last_name = 'Bennett'
user1.email = 'dbennettksm58@gmail.com'
user1.street_address = '653 N 100 W'
user1.city = 'Provo'
user1.state = 'Utah'
user1.zip_code = '84601'
user1.phone = '4357054535'
user1.birthday = '1993-07-09'
user1.set_password('password')
user1.username = '1'
user1.date_joined = datetime(2017, 6, 1, 5, 0)
user1.last_login = datetime(2017, 6, 1, 5, 0)
user1.save()



user2 = FomoUser()
user2.first_name = 'Chris'
user2.last_name = 'Pratt'
user2.email = 'coolpersoncp@gmail.com'
user2.street_address = '458 N 200 W'
user2.city = 'Provo'
user2.state = 'Utah'
user2.zip_code = '84604'
user2.phone = '8167895645'
user2.birthday = '1993-07-09'
user2.set_password('password')
user2.username = '2'
user2.date_joined = datetime(2017, 6, 1, 5, 0)
user2.last_login = datetime(2017, 6, 1, 5, 0)
user2.save()


user3 = FomoUser()
user3.first_name = 'Michael'
user3.last_name = 'Jordan'
user3.email = 'mj@gmail.com'
user3.street_address = '526 N 200 W'
user3.city = 'Raleigh'
user3.state = 'North Carolina'
user3.zip_code = '48565'
user3.phone = '8548956565'
user3.birthday = '1993-07-09'
user3.set_password('password')
user3.username = '3'
user3.date_joined = datetime(2017, 6, 1, 5, 0)
user3.last_login = datetime(2017, 6, 1, 5, 0)
user3.save()

user4 = FomoUser()
user4.first_name = 'Lebron'
user4.last_name = 'James'
user4.email = 'kingjames@gmail.com'
user4.street_address = '456 N 800 W'
user4.city = 'Akron'
user4.state = 'Ohio'
user4.zip_code = '85654'
user4.phone = '4564588965'
user4.birthday = '1993-07-09'
user4.set_password('password')
user4.username = '4'
user4.date_joined = datetime(2017, 6, 1, 5, 0)
user4.last_login = datetime(2017, 6, 1, 5, 0)
user4.save()

u11 = FomoUser.objects.get(id=3)
print(u11.first_name)

#Filter will print out all the ids that has a username of Devin
u22 = FomoUser.objects.filter(username = 'Devin')
for U in u22:
    print(U.id)


#Exclude will print out u2,u3,u4, except Devin
u33 = FomoUser.objects.exclude(username='Devin')
for U1 in u33:
    print(U1.id)
