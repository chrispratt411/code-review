

def LastFiveMiddleWare(get_response):
    #one time configuration initialization
    def middleware(request):

        print('<<<<<<<<<<<<<<<<<<<<< middleware called at beginning')
        
        last5_list = request.session.get('last5')
        if last5_list is None:
            last5_list = []
        request.last5 = last5_list

        response = get_response(request)

        print('!!!!!!!!!!!! middleware called at end')
        request.session['last5'] = request.last5
        return response
    return middleware
