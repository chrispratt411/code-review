from django.conf import settings
from django_mako_plus import view_function
from .. import dmp_render, dmp_render_to_string
from datetime import datetime
from catalog import models as cmod
import random

@view_function
def process_request(request):
    print('rendering product details')
    try:
        #product = cmod.Product.objects.get(id=request.GET.get('pid'))
        product = cmod.Product.objects.get(id=request.urlparams[0])
    except cmod.Product.DoesNotExist:
        return HttpResponseRedirect('/manager/products')

    #an idea


    request.last5.insert(0, request.urlparams[0])
    print(request.urlparams[0],'devin is smart',request.last5[0])
    print(request.last5)

    # check to see if the list is bigger than 5.
    while (len(request.last5) > 5):
        print('did a pop!!')
        request.last5.pop()
        print(request.last5)
    request.session['last5'] = request.last5
    products = list(map(lambda item: cmod.Product.objects.get(id=item), request.last5))
    context = {
        'product':product,
        'products':products,
    }
    return dmp_render(request, 'productDetails.html', context)
