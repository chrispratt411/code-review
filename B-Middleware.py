def LastFiveMiddleware(get_response):
    # one-time configuration and initialization
    def middleware(request):
        # code to be executed for each request before the view (and later middleware) are called
        print('>>>>>> LastFiveMiddleware called')

        # pull the last 5 from request.session
        last5_list = request.session.get('last5')
        if last5_list is None:
            last5_list = []
        request.last5 = last5_list

        # let django continue
        response = get_response(request)

        request.session['last5'] = request.last5[:6]

        # to access on another page      'request.last5'


        return response
    return middleware
